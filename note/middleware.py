#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
import json
import keep_alive
import nk

class WhoAmiMiddleware(object):
    """Met à jour les données utilisateurs en live"""

    def process_response(self, request, response):
        """Let's hijack a session"""

        idbde = request.session.get("whoami", {}).get('idbde', None)

        if idbde is None:
            return response

        # On récupère la socket dans keep_alive.CONNS
        if idbde in keep_alive.CONNS:
            sock = keep_alive.CONNS[idbde]
        else:
            messages.add_error(request, messages.ERRMSG_NOSOCKET)
            return (False, HttpResponseRedirect(_gen_redirect_postlogin(request)))

        sock.write(json.dumps(["whoami"]))
        out = nk.full_read(sock)
        whoami = out["msg"]
        request.session["whoami"] = whoami

        return response
