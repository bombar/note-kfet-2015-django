#!/usr/bin/env python
# -*- encoding: utf-8 -*-

# Templates django custom pour la Note Kfet 2015

from django import template
from note import settings

from collections import namedtuple
import itertools
import time
import random

register = template.Library()

@register.simple_tag
def euro(montant):
    """Affiche une somme en euros,
       avec le bon nombre de chiffres après la virgule"""
    try:
        if isinstance(montant, int):
            return (u"%.2f\xa0€" % (montant/100.0)).replace(".", ",")
        elif isinstance(montant, float):
            return (u"%.2f\xa0€" % (montant)).replace(".", ",")
    except:
        return u''

@register.simple_tag
def render_datetime(timestamp):
    """Affiche une date en changeant son format (cf settings.py)"""
    try:
        timestamp = timestamp.rsplit(".", 1)[0] # pour se débarasser des éventuelles microsecondes
        date = time.strptime(timestamp, settings.DATETIME_INPUT_FORMAT)
        date_format = str(settings.DATETIME_RENDERING_FORMAT.encode('utf-8'))
        date = time.strftime(date_format, date)
        return date
    except:
        return ''

@register.filter
def groupby(items, num):
    """Pour lister par blocks de ``num`` éléments."""
    args = [iter(items)] * num
    return itertools.izip_longest(*args)

@register.filter
def buttons_groupby(items, double):
    """Filtre pour lister par blocks de 6 éléments quand on est en mode double stack, par 3 sinon."""
    if double:
        return groupby(items, 6)
    else:
        return groupby(items, 3)

@register.inclusion_tag('note/template_activite.html')
def render_activite(activite, isadmin=False, isgestion=False, ismine=False, hasnote=False):
    """Affiche le tableau d'une activité"""
    Link = namedtuple('Link', ['label', 'target', 'style'])
    boutons = []

    if isadmin:
        if not isgestion:
            boutons.append(Link(
                    'Gérer',
                    '%sactivites/%s/gestion/' % (settings.NOTE_ROOT_URL, activite["id"]),
                    'info'))
    if isgestion:
        if activite["valideparpseudo"]:
            boutons.append(Link(
                    'Dévalider',
                    '%sactivites/%s/gestion/invalidate/' % (settings.NOTE_ROOT_URL, activite["id"]),
                    'warning'))

            if activite["liste"]:
                if activite["ouvert"]:
                    boutons.append(Link(
                        'Fermer',
                        '%sactivites/%s/gestion/close' % (settings.NOTE_ROOT_URL, activite["id"]),
                        'warning'))
                else:
                    boutons.append(Link(
                        'Ouvrir',
                        '%sactivites/%s/gestion/open' % (settings.NOTE_ROOT_URL, activite["id"]),
                        'success'))

        else:
            boutons.append(Link(
                    'Valider',
                    '%sactivites/%s/gestion/validate/' % (settings.NOTE_ROOT_URL, activite["id"]),
                    'success'))
    if hasnote and activite["ouvert"] and activite["liste"] and not isadmin:
        boutons.append(Link(
                "Accéder à la page d'entrée",
                "%sactivites/%s/entree/" % (settings.NOTE_ROOT_URL, activite["id"]),
                "success"))

    if activite["liste"] and (isadmin or "caninvite" in activite and activite["caninvite"]):
        boutons.append(Link(
                'Inviter',
                '%sactivites/%s/%s' % (settings.NOTE_ROOT_URL, activite["id"], "" if not isadmin else "admin"),
                'warning' if (isadmin and activite["fails"]) else 'primary'))

    if (ismine and activite["validepar"] is None) or isgestion:
        boutons.append(Link(
                'Modifier',
                '%s%sactivites/%s/%s' % (settings.NOTE_ROOT_URL, "mes_" if ismine else "", activite["id"], "gestion/modifier/" if not ismine else ""),
                'primary'))

    if "candelete" in activite and activite["candelete"]:
        boutons.append(Link(
                'Supprimer',
                '%s%sactivites/%s/%sdelete/' % (settings.NOTE_ROOT_URL, "mes_" if ismine else "", activite["id"], "gestion/" if isgestion else ""),
                'danger'))

    return {
        'NOTE_ROOT_URL' : settings.NOTE_ROOT_URL,
        'activite': activite,
        'boutons': boutons,
        'isadmin': isadmin,
        'isgestion': isgestion,
        'ismine': ismine,
        }

@register.filter
def bootstrap_level(value):
    """
        Transforme un level de warning Django vers le niveau équivalent pour
        Bootstrap
    """
    if value == 'error':
        return 'danger'

    return value

@register.filter
def field_type(object):
    """ Tag destiné à récupérer le type de field."""
    return object.field.widget.__class__.__name__

@register.filter
def shuffle(arg):
    tmp = list(arg)[:]
    random.shuffle(tmp)
    return tmp

@register.assignment_tag
def alias(obj):
    """
    Alias Tag
    """
    return obj
